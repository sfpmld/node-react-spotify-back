// Enhance the express Request to add business payload
declare namespace Express {
  export interface Request {
    userId?: string;
    sessionId?: string;
    token?:string;
  }
}
