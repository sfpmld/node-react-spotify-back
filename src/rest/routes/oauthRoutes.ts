import { Router } from '../../helpers';
import makeExpressCallback from '../../helpers/makeExpressCallback';
import { oauthLogin, oauthLogged } from '../../rest';

const router = Router.getInstance();

router.get('/oauth/login', oauthLogin);

router.get('/oauth/logged', makeExpressCallback(oauthLogged));

export default router;
