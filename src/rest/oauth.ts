import querystring from 'querystring';
import { oauth_login_url } from '../constants';
import { AuthData } from '../data-access/AuthData';
import { ControllerType, logger, Response } from '../helpers';
import { tryPromise } from '../helpers';
import { client_url } from '../constants';
import { httpStatusCode } from '../constants';

export const oauthLogin: ControllerType = (_req, res, _next) => {
  res.redirect(oauth_login_url);
  logger.info('oauth login!');
};

export const oauthLogged = async ({
  code,
  res,
}: {
  code: string;
  res: Response;
}) => {
  const authData = new AuthData();
  let token = await tryPromise(authData.getToken(code));
  logger.info('oauth logged!');

  const { access_token } = token.data;

  // use the access token to access the Spotify Web API
  const userProfile = await tryPromise(authData.getUserInfos(access_token));
  logger.info('user infos loaded!');

  res.status(httpStatusCode.REDIRECTING).redirect(
    client_url +
      '/?' +
      querystring.stringify({
        access_token: token.data.access_token,
        refresh_token: token.data.refresh_token,
        expires_in: token.data.expires_in,
        user: userProfile.data.id,
      })
  );
  return;
};
