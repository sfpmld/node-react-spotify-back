import { AppType, ControllerType } from '../helpers';
import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import { logger } from '../helpers';
import { loggerHeadersBlackList, loggerQueryBlackList } from '../constants';

const loggerMiddleware: ControllerType = (req, _res, next) => {
  // copy http obects values
  const http_request_method = req.method;
  const http_request_path = req.path;
  const http_request_headers = { ...req.headers };
  const http_request_params = { ...req.params };
  const http_request_query = { ...req.query };
  // removing sensitive informations from http request object
  loggerHeadersBlackList.forEach(toRemove => {
    delete http_request_headers[toRemove];
  });
  loggerQueryBlackList.forEach(toRemove => {
    delete http_request_query[toRemove];
  });

  logger.info(
    JSON.stringify({
      http_request_method,
      http_request_path,
      http_request_headers,
      http_request_params,
      http_request_query
    })
  );

  next();
};

const commonMiddlewares = (app: AppType) => {
  // x-www-form-urlencoded <form>
  app.use(bodyParser.urlencoded({ extended: true })),
  // application/json
  app.use(bodyParser.json());
  // Security
  app.use(helmet());
  // CORS Error Handling
  const corsOptions = {
    origin: true,
    credentials: true,
    methods: "['OPTIONS','GET','HEAD','PUT','PATCH','POST','DELETE']",
    allowHeaders: "['Content-Type', 'Authorization']",
    optionsSuccessStatus: 200 /* some legacy browsers (IE11, various SmartTVs) choke on 204 */,
    preflightContinue: false,
  };

  app.use(cors(corsOptions));

  // logger
  app.use(loggerMiddleware);
};

export { commonMiddlewares };
