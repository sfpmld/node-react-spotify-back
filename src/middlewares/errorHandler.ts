import { ErrorRequestHandler, Express } from 'express';

import { isErrorType } from '../errors';
import { logger } from '../helpers';
import { httpStatusCode } from '../constants';

const errorLogger: ErrorRequestHandler = (error, _req, _res, next) => {
  if (error.message) {
    logger.error(error.message);
  }
  if (error.stack) {
    logger.error(error.stack);
  }
  next(error);
};

export const authenticationErrorHandler: ErrorRequestHandler = (
  error,
  _req,
  res,
  next
) => {
  if (isErrorType.authentication(error)) {
    return res.sendStatus(httpStatusCode.UNHAUTORIZED);
  }
  next(error);
};

export const accessDeniedErrorHandler: ErrorRequestHandler = (
  error,
  _req,
  res,
  next
) => {
  if (isErrorType.accessDenied(error)) {
    return res.sendStatus(httpStatusCode.FORBIDDEN);
  }
  next(error);
};

export const internalErrorHandler: ErrorRequestHandler = (
  error,
  _req,
  res,
  next
) => {
  if (isErrorType.internal(error)) {
    return res.sendStatus(httpStatusCode.HTTP_INTERNAL_ERROR);
  }
  next(error);
};

const genericErrorHandler: ErrorRequestHandler = (error, _req, res, next) => {
  const statusCode = error.status || httpStatusCode.HTTP_INTERNAL_ERROR;
  const message =
    error.status === httpStatusCode.HTTP_INTERNAL_ERROR
      ? httpStatusCode.HTTP_INTERNAL_ERROR
      : error.message;

  res.status(statusCode).json({
    data: {},
    error: {
      message,
      status: statusCode,
    },
  });
  next();
};

export const errorHandlingMiddleware = (app: Express) => {
  app.use([
    errorLogger,
    authenticationErrorHandler,
    accessDeniedErrorHandler,
    internalErrorHandler,
    genericErrorHandler,
  ]);
};
