export const objectFieldTypes = (obj: Object) => {
  return Object.values(obj).map(v =>
    Array.isArray(v) ? 'object[]' : typeof v
  );
};
