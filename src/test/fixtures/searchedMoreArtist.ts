export const searchedMoreArtist = {
  data: {
  "artists": {
    "href": "https://api.spotify.com/v1/search?query=Miles+davis&type=artist&offset=20&limit=20",
    "items": [
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/0aZOia8oRWCbjW7X2196yW"
        },
        "followers": {
          "href": null,
          "total": 2
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/0aZOia8oRWCbjW7X2196yW",
        "id": "0aZOia8oRWCbjW7X2196yW",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b27352c6bb9d832a008967ff3418",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e0252c6bb9d832a008967ff3418",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d0000485152c6bb9d832a008967ff3418",
            "width": 64
          }
        ],
        "name": "Miles Davish",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:0aZOia8oRWCbjW7X2196yW"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/4Gqrai5JttW8ymETbzAZ1b"
        },
        "followers": {
          "href": null,
          "total": 40
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/4Gqrai5JttW8ymETbzAZ1b",
        "id": "4Gqrai5JttW8ymETbzAZ1b",
        "images": [],
        "name": "Davis,Miles",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:4Gqrai5JttW8ymETbzAZ1b"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/2ErnvSRzCMFXpFTVJtj3kc"
        },
        "followers": {
          "href": null,
          "total": 3
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/2ErnvSRzCMFXpFTVJtj3kc",
        "id": "2ErnvSRzCMFXpFTVJtj3kc",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b2738f6e8a38c708c582346920cb",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e028f6e8a38c708c582346920cb",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d000048518f6e8a38c708c582346920cb",
            "width": 64
          }
        ],
        "name": "Ylond Miles-Davis",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:2ErnvSRzCMFXpFTVJtj3kc"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/2xxCnRO0Dsp1wl4p7f4n5R"
        },
        "followers": {
          "href": null,
          "total": 4
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/2xxCnRO0Dsp1wl4p7f4n5R",
        "id": "2xxCnRO0Dsp1wl4p7f4n5R",
        "images": [],
        "name": "Davis Miles Jr",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:2xxCnRO0Dsp1wl4p7f4n5R"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/3BHHCFITkc46SxJY15sERW"
        },
        "followers": {
          "href": null,
          "total": 3
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/3BHHCFITkc46SxJY15sERW",
        "id": "3BHHCFITkc46SxJY15sERW",
        "images": [],
        "name": "Miles B. Davis",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:3BHHCFITkc46SxJY15sERW"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/6iNCSVor4pytym88ptNCwF"
        },
        "followers": {
          "href": null,
          "total": 2
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/6iNCSVor4pytym88ptNCwF",
        "id": "6iNCSVor4pytym88ptNCwF",
        "images": [],
        "name": "Miles Dewey Davis III",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:6iNCSVor4pytym88ptNCwF"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/3lKKB38udSbK8unhSlhwIF"
        },
        "followers": {
          "href": null,
          "total": 4
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/3lKKB38udSbK8unhSlhwIF",
        "id": "3lKKB38udSbK8unhSlhwIF",
        "images": [],
        "name": "George Gershwin & Miles Davis",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:3lKKB38udSbK8unhSlhwIF"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/1Me5uqmDKqxVpEsuI6Kk0J"
        },
        "followers": {
          "href": null,
          "total": 21
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/1Me5uqmDKqxVpEsuI6Kk0J",
        "id": "1Me5uqmDKqxVpEsuI6Kk0J",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b2735616ca478ba7bac0f85b24c3",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e025616ca478ba7bac0f85b24c3",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d000048515616ca478ba7bac0f85b24c3",
            "width": 64
          }
        ],
        "name": "Miles Davis and his favorite Tenors",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:1Me5uqmDKqxVpEsuI6Kk0J"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/6VRIlYasoFtUyCVhy6J5et"
        },
        "followers": {
          "href": null,
          "total": 33
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/6VRIlYasoFtUyCVhy6J5et",
        "id": "6VRIlYasoFtUyCVhy6J5et",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b2734aa136e48b47ad7454d33f10",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e024aa136e48b47ad7454d33f10",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d000048514aa136e48b47ad7454d33f10",
            "width": 64
          }
        ],
        "name": "Miles Davis & Jackie McLean",
        "popularity": 16,
        "type": "artist",
        "uri": "spotify:artist:6VRIlYasoFtUyCVhy6J5et"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/4p9UOqHs3bOP33UGbjUdOR"
        },
        "followers": {
          "href": null,
          "total": 71
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/4p9UOqHs3bOP33UGbjUdOR",
        "id": "4p9UOqHs3bOP33UGbjUdOR",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b273dc0cda61d953d9885b4e1d56",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e02dc0cda61d953d9885b4e1d56",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d00004851dc0cda61d953d9885b4e1d56",
            "width": 64
          }
        ],
        "name": "Charlie Parker Quintet feat. Miles Davis",
        "popularity": 8,
        "type": "artist",
        "uri": "spotify:artist:4p9UOqHs3bOP33UGbjUdOR"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/6XqVCJS2eioGKcneaZ3IAQ"
        },
        "followers": {
          "href": null,
          "total": 10
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/6XqVCJS2eioGKcneaZ3IAQ",
        "id": "6XqVCJS2eioGKcneaZ3IAQ",
        "images": [],
        "name": "Miles Davis (Trumpet)",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:6XqVCJS2eioGKcneaZ3IAQ"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/18votPz9tH4npulYjjuiDt"
        },
        "followers": {
          "href": null,
          "total": 325
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/18votPz9tH4npulYjjuiDt",
        "id": "18votPz9tH4npulYjjuiDt",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b273d7cb54bb9e0e7fec6526c82c",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e02d7cb54bb9e0e7fec6526c82c",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d00004851d7cb54bb9e0e7fec6526c82c",
            "width": 64
          }
        ],
        "name": "Miles Davis, John Coltrane",
        "popularity": 5,
        "type": "artist",
        "uri": "spotify:artist:18votPz9tH4npulYjjuiDt"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/2JJKhxawYiJFNomNLn0TCd"
        },
        "followers": {
          "href": null,
          "total": 15
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/2JJKhxawYiJFNomNLn0TCd",
        "id": "2JJKhxawYiJFNomNLn0TCd",
        "images": [],
        "name": "Lee Konitz, Miles Davis",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:2JJKhxawYiJFNomNLn0TCd"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/2dEfkpJMtuNnuOGV7FjbSY"
        },
        "followers": {
          "href": null,
          "total": 23
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/2dEfkpJMtuNnuOGV7FjbSY",
        "id": "2dEfkpJMtuNnuOGV7FjbSY",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b2734c15c13a282ab43ab6028d34",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e024c15c13a282ab43ab6028d34",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d000048514c15c13a282ab43ab6028d34",
            "width": 64
          }
        ],
        "name": "Michel Legrand|Miles Davis",
        "popularity": 2,
        "type": "artist",
        "uri": "spotify:artist:2dEfkpJMtuNnuOGV7FjbSY"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/0SE9YqhoqApJzEyPREr1jE"
        },
        "followers": {
          "href": null,
          "total": 5
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/0SE9YqhoqApJzEyPREr1jE",
        "id": "0SE9YqhoqApJzEyPREr1jE",
        "images": [],
        "name": "Miles Davis, Charlie Parker's Reboppers",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:0SE9YqhoqApJzEyPREr1jE"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/0TVCaZgJimPgff78mJ3W03"
        },
        "followers": {
          "href": null,
          "total": 22
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/0TVCaZgJimPgff78mJ3W03",
        "id": "0TVCaZgJimPgff78mJ3W03",
        "images": [],
        "name": "John Coltrane, Miles Davis Quintet",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:0TVCaZgJimPgff78mJ3W03"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/0Z5zHuaOcG43OwPfNXLmCY"
        },
        "followers": {
          "href": null,
          "total": 7
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/0Z5zHuaOcG43OwPfNXLmCY",
        "id": "0Z5zHuaOcG43OwPfNXLmCY",
        "images": [],
        "name": "Miles Davis, Charlie Parker Quintet",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:0Z5zHuaOcG43OwPfNXLmCY"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/1q0knbus6n1mEZX1kqz957"
        },
        "followers": {
          "href": null,
          "total": 5
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/1q0knbus6n1mEZX1kqz957",
        "id": "1q0knbus6n1mEZX1kqz957",
        "images": [],
        "name": "Miles Davis, Charlie Parker Septet",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:1q0knbus6n1mEZX1kqz957"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/2N1TGs1eXdgaafAjpYCE8z"
        },
        "followers": {
          "href": null,
          "total": 738
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/2N1TGs1eXdgaafAjpYCE8z",
        "id": "2N1TGs1eXdgaafAjpYCE8z",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b2731cdefbbd8bde73a5509a2913",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e021cdefbbd8bde73a5509a2913",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d000048511cdefbbd8bde73a5509a2913",
            "width": 64
          }
        ],
        "name": "Miles Davis & John Coltrane",
        "popularity": 3,
        "type": "artist",
        "uri": "spotify:artist:2N1TGs1eXdgaafAjpYCE8z"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/2RvQgRqpGanwMigTmFQcI3"
        },
        "followers": {
          "href": null,
          "total": 8
        },
        "genres": [],
        "href": "https://api.spotify.com/v1/artists/2RvQgRqpGanwMigTmFQcI3",
        "id": "2RvQgRqpGanwMigTmFQcI3",
        "images": [],
        "name": "Jimmy Forrest & Miles Davis",
        "popularity": 0,
        "type": "artist",
        "uri": "spotify:artist:2RvQgRqpGanwMigTmFQcI3"
      }
    ],
    "limit": 20,
    "next": "https://api.spotify.com/v1/search?query=Miles+davis&type=artist&offset=40&limit=20",
    "offset": 20,
    "previous": "https://api.spotify.com/v1/search?query=Miles+davis&type=artist&offset=0&limit=20",
    "total": 88
  }
}
}


