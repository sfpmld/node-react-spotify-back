export const artistById = {
  data: {
    "external_urls": {
      "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
    },
    "followers": {
      "href": null,
      "total": 193251
    },
    "genres": [
      "bebop",
      "contemporary post-bop",
      "cool jazz",
      "hard bop",
      "jazz",
      "jazz fusion"
    ],
    "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
    "id": "71Ur25Abq58vksqJINpGdx",
    "images": [
      {
        "height": 723,
        "url": "https://i.scdn.co/image/7e2b6815b5973d47cccbad1d834089231d9b737e",
        "width": 999
      },
      {
        "height": 463,
        "url": "https://i.scdn.co/image/399ee45e619648be6ce992228b79354f31b69999",
        "width": 640
      },
      {
        "height": 145,
        "url": "https://i.scdn.co/image/9c69550b77ce6f3d040f32122bff0325e0dddd3c",
        "width": 200
      },
      {
        "height": 46,
        "url": "https://i.scdn.co/image/5b89bd24901e2d12347296e753027b70e0bfeacd",
        "width": 64
      }
    ],
    "name": "Miles Davis Quintet",
    "popularity": 53,
    "type": "artist",
    "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
  }
}
