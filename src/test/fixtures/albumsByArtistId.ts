export const albumsByArtistId = {
  data: {
  "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx/albums?offset=0&limit=20&include_groups=album,single,compilation,appears_on",
  "items": [
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AD",
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BH",
        "BO",
        "BR",
        "CH",
        "CL",
        "CO",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "HK",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JO",
        "JP",
        "KW",
        "LB",
        "LI",
        "LT",
        "LU",
        "LV",
        "MA",
        "MC",
        "MT",
        "MY",
        "NL",
        "NO",
        "NZ",
        "OM",
        "PE",
        "PH",
        "PL",
        "PS",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "TH",
        "TN",
        "TR",
        "TW",
        "UY",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/4cGwXL4TKQ64RDlNTDeVCy"
      },
      "href": "https://api.spotify.com/v1/albums/4cGwXL4TKQ64RDlNTDeVCy",
      "id": "4cGwXL4TKQ64RDlNTDeVCy",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b2733632f3c3d53b5cd218bed414",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e023632f3c3d53b5cd218bed414",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d000048513632f3c3d53b5cd218bed414",
          "width": 64
        }
      ],
      "name": "Live In Rome & Copenhagen 1969",
      "release_date": "1969",
      "release_date_precision": "year",
      "total_tracks": 15,
      "type": "album",
      "uri": "spotify:album:4cGwXL4TKQ64RDlNTDeVCy"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AD",
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BH",
        "BO",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JO",
        "JP",
        "KW",
        "LB",
        "LI",
        "LT",
        "LU",
        "LV",
        "MA",
        "MC",
        "MT",
        "MX",
        "MY",
        "NI",
        "NL",
        "NO",
        "NZ",
        "OM",
        "PA",
        "PE",
        "PH",
        "PL",
        "PS",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TH",
        "TN",
        "TR",
        "TW",
        "UY",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/5UcOJuwmsNWEpOs9NS4OQs"
      },
      "href": "https://api.spotify.com/v1/albums/5UcOJuwmsNWEpOs9NS4OQs",
      "id": "5UcOJuwmsNWEpOs9NS4OQs",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273c38787d5268ecee8abc76b11",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02c38787d5268ecee8abc76b11",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851c38787d5268ecee8abc76b11",
          "width": 64
        }
      ],
      "name": "Live in Berlin 1969",
      "release_date": "1969",
      "release_date_precision": "year",
      "total_tracks": 9,
      "type": "album",
      "uri": "spotify:album:5UcOJuwmsNWEpOs9NS4OQs"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AD",
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BH",
        "BO",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JO",
        "JP",
        "KW",
        "LB",
        "LI",
        "LT",
        "LU",
        "LV",
        "MA",
        "MC",
        "MT",
        "MX",
        "MY",
        "NI",
        "NL",
        "NO",
        "NZ",
        "OM",
        "PA",
        "PE",
        "PH",
        "PL",
        "PS",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TH",
        "TN",
        "TR",
        "TW",
        "UY",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/5c1tJPu9wPGE8zv7seexfd"
      },
      "href": "https://api.spotify.com/v1/albums/5c1tJPu9wPGE8zv7seexfd",
      "id": "5c1tJPu9wPGE8zv7seexfd",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273a6ca2ffe63b4e6755c61a04a",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02a6ca2ffe63b4e6755c61a04a",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851a6ca2ffe63b4e6755c61a04a",
          "width": 64
        }
      ],
      "name": "Winter In Europe 1967",
      "release_date": "1967",
      "release_date_precision": "year",
      "total_tracks": 12,
      "type": "album",
      "uri": "spotify:album:5c1tJPu9wPGE8zv7seexfd"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AD",
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BO",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DO",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JP",
        "LI",
        "LT",
        "LU",
        "LV",
        "MC",
        "MT",
        "MX",
        "MY",
        "NI",
        "NL",
        "NO",
        "NZ",
        "PA",
        "PE",
        "PH",
        "PL",
        "PT",
        "PY",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TH",
        "TN",
        "TR",
        "TW",
        "US",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/0Q0bftWuBSwZAHBKZr0lxB"
      },
      "href": "https://api.spotify.com/v1/albums/0Q0bftWuBSwZAHBKZr0lxB",
      "id": "0Q0bftWuBSwZAHBKZr0lxB",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273918d4376b1b56318fcc728ca",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02918d4376b1b56318fcc728ca",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851918d4376b1b56318fcc728ca",
          "width": 64
        }
      ],
      "name": "Steamin' With The Miles Davis Quintet",
      "release_date": "1961",
      "release_date_precision": "year",
      "total_tracks": 6,
      "type": "album",
      "uri": "spotify:album:0Q0bftWuBSwZAHBKZr0lxB"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AD",
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BH",
        "BO",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JO",
        "JP",
        "KW",
        "LB",
        "LI",
        "LT",
        "LU",
        "LV",
        "MA",
        "MC",
        "MT",
        "MX",
        "MY",
        "NI",
        "NL",
        "NO",
        "NZ",
        "OM",
        "PA",
        "PE",
        "PH",
        "PL",
        "PS",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TH",
        "TN",
        "TR",
        "TW",
        "UY",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/3V4p5nSCaiBB79ThluzJMU"
      },
      "href": "https://api.spotify.com/v1/albums/3V4p5nSCaiBB79ThluzJMU",
      "id": "3V4p5nSCaiBB79ThluzJMU",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b2738310989d247fedb2f9c67c52",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e028310989d247fedb2f9c67c52",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d000048518310989d247fedb2f9c67c52",
          "width": 64
        }
      ],
      "name": "Manchester Concert-Complete 1960 Live At The Free Trade Hall",
      "release_date": "1960",
      "release_date_precision": "year",
      "total_tracks": 13,
      "type": "album",
      "uri": "spotify:album:3V4p5nSCaiBB79ThluzJMU"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AD",
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BH",
        "BO",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JO",
        "JP",
        "KW",
        "LB",
        "LI",
        "LT",
        "LU",
        "LV",
        "MA",
        "MC",
        "MT",
        "MX",
        "MY",
        "NI",
        "NL",
        "NO",
        "NZ",
        "OM",
        "PA",
        "PE",
        "PH",
        "PL",
        "PS",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TH",
        "TN",
        "TR",
        "TW",
        "US",
        "UY",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/7buLIJn2VuqsVORghMEvli"
      },
      "href": "https://api.spotify.com/v1/albums/7buLIJn2VuqsVORghMEvli",
      "id": "7buLIJn2VuqsVORghMEvli",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273f4a69b1c7272d434ebe91e85",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02f4a69b1c7272d434ebe91e85",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851f4a69b1c7272d434ebe91e85",
          "width": 64
        }
      ],
      "name": "Workin'",
      "release_date": "1959",
      "release_date_precision": "year",
      "total_tracks": 8,
      "type": "album",
      "uri": "spotify:album:7buLIJn2VuqsVORghMEvli"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "CA",
        "MX",
        "US"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/1mGkIZVToeCNAseve9I1nv"
      },
      "href": "https://api.spotify.com/v1/albums/1mGkIZVToeCNAseve9I1nv",
      "id": "1mGkIZVToeCNAseve9I1nv",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b27390fe0cc71331245296467fdf",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e0290fe0cc71331245296467fdf",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d0000485190fe0cc71331245296467fdf",
          "width": 64
        }
      ],
      "name": "Workin' With The Miles Davis Quintet",
      "release_date": "1959",
      "release_date_precision": "year",
      "total_tracks": 8,
      "type": "album",
      "uri": "spotify:album:1mGkIZVToeCNAseve9I1nv"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "CA",
        "US"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/4B4m8bgL077bvRfK2bGVzn"
      },
      "href": "https://api.spotify.com/v1/albums/4B4m8bgL077bvRfK2bGVzn",
      "id": "4B4m8bgL077bvRfK2bGVzn",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b2736cc788e572ec938018dcabf4",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e026cc788e572ec938018dcabf4",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d000048516cc788e572ec938018dcabf4",
          "width": 64
        }
      ],
      "name": "Workin' With The Miles Davis Quintet",
      "release_date": "1959",
      "release_date_precision": "year",
      "total_tracks": 8,
      "type": "album",
      "uri": "spotify:album:4B4m8bgL077bvRfK2bGVzn"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AD",
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BH",
        "BO",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JO",
        "JP",
        "KW",
        "LB",
        "LI",
        "LT",
        "LU",
        "LV",
        "MA",
        "MC",
        "MT",
        "MX",
        "MY",
        "NI",
        "NL",
        "NO",
        "NZ",
        "OM",
        "PA",
        "PE",
        "PH",
        "PL",
        "PS",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TH",
        "TN",
        "TR",
        "TW",
        "US",
        "UY",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/0dyIXPKoUBt1vFJHX57dqt"
      },
      "href": "https://api.spotify.com/v1/albums/0dyIXPKoUBt1vFJHX57dqt",
      "id": "0dyIXPKoUBt1vFJHX57dqt",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273ab2083ab4b97f7948ff163a1",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02ab2083ab4b97f7948ff163a1",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851ab2083ab4b97f7948ff163a1",
          "width": 64
        }
      ],
      "name": "Relaxin' With The Miles Davis Quintet",
      "release_date": "1958",
      "release_date_precision": "year",
      "total_tracks": 6,
      "type": "album",
      "uri": "spotify:album:0dyIXPKoUBt1vFJHX57dqt"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "CA",
        "GB",
        "MX",
        "US"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/1h7NtOd5fdBQEjJbB9Wk2B"
      },
      "href": "https://api.spotify.com/v1/albums/1h7NtOd5fdBQEjJbB9Wk2B",
      "id": "1h7NtOd5fdBQEjJbB9Wk2B",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273baf1d0c09715ad4631e75770",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02baf1d0c09715ad4631e75770",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851baf1d0c09715ad4631e75770",
          "width": 64
        }
      ],
      "name": "Relaxin' With The Miles Davis Quintet",
      "release_date": "1958",
      "release_date_precision": "year",
      "total_tracks": 6,
      "type": "album",
      "uri": "spotify:album:1h7NtOd5fdBQEjJbB9Wk2B"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "CA",
        "US"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/1mQCpyWoI4vTore21DgOQG"
      },
      "href": "https://api.spotify.com/v1/albums/1mQCpyWoI4vTore21DgOQG",
      "id": "1mQCpyWoI4vTore21DgOQG",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273e7071b7adfc3a494142d7c71",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02e7071b7adfc3a494142d7c71",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851e7071b7adfc3a494142d7c71",
          "width": 64
        }
      ],
      "name": "Relaxin' With The Miles Davis Quintet",
      "release_date": "1958",
      "release_date_precision": "year",
      "total_tracks": 6,
      "type": "album",
      "uri": "spotify:album:1mQCpyWoI4vTore21DgOQG"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CZ",
        "DE",
        "DK",
        "EE",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "HK",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JP",
        "LT",
        "LU",
        "LV",
        "MX",
        "MY",
        "NL",
        "NO",
        "NZ",
        "PH",
        "PL",
        "PT",
        "SA",
        "SE",
        "SG",
        "SK",
        "TH",
        "TN",
        "TR",
        "TW",
        "US",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/3BvuVBfJyM4SxgsM3PN3JD"
      },
      "href": "https://api.spotify.com/v1/albums/3BvuVBfJyM4SxgsM3PN3JD",
      "id": "3BvuVBfJyM4SxgsM3PN3JD",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273c658f3a567a8291b022634bb",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02c658f3a567a8291b022634bb",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851c658f3a567a8291b022634bb",
          "width": 64
        }
      ],
      "name": "Cookin' With The Miles Davis Quintet",
      "release_date": "1957",
      "release_date_precision": "year",
      "total_tracks": 4,
      "type": "album",
      "uri": "spotify:album:3BvuVBfJyM4SxgsM3PN3JD"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CZ",
        "DE",
        "DK",
        "EE",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "HK",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JP",
        "LT",
        "LU",
        "LV",
        "MX",
        "MY",
        "NL",
        "NO",
        "NZ",
        "PH",
        "PL",
        "PT",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "TH",
        "TN",
        "TR",
        "TW",
        "US",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/6QPFCq6SHAOhBI1Vf14G0y"
      },
      "href": "https://api.spotify.com/v1/albums/6QPFCq6SHAOhBI1Vf14G0y",
      "id": "6QPFCq6SHAOhBI1Vf14G0y",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b2732a2d01f78d82ad4d8c095ab1",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e022a2d01f78d82ad4d8c095ab1",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d000048512a2d01f78d82ad4d8c095ab1",
          "width": 64
        }
      ],
      "name": "Cookin' With The Miles Davis Quintet",
      "release_date": "1957",
      "release_date_precision": "year",
      "total_tracks": 4,
      "type": "album",
      "uri": "spotify:album:6QPFCq6SHAOhBI1Vf14G0y"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AD",
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BH",
        "BO",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JO",
        "JP",
        "KW",
        "LB",
        "LI",
        "LT",
        "LU",
        "LV",
        "MA",
        "MC",
        "MT",
        "MX",
        "MY",
        "NI",
        "NL",
        "NO",
        "NZ",
        "OM",
        "PA",
        "PE",
        "PH",
        "PL",
        "PS",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TH",
        "TN",
        "TR",
        "TW",
        "US",
        "UY",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/50J57uCvPub0yK2kjkfC9J"
      },
      "href": "https://api.spotify.com/v1/albums/50J57uCvPub0yK2kjkfC9J",
      "id": "50J57uCvPub0yK2kjkfC9J",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273ae63e56e4df8116f286c9750",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02ae63e56e4df8116f286c9750",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851ae63e56e4df8116f286c9750",
          "width": 64
        }
      ],
      "name": "Cookin' With The Miles Davis Quintet",
      "release_date": "1957",
      "release_date_precision": "year",
      "total_tracks": 4,
      "type": "album",
      "uri": "spotify:album:50J57uCvPub0yK2kjkfC9J"
    },
    {
      "album_group": "album",
      "album_type": "album",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AD",
        "AE",
        "AR",
        "AT",
        "BE",
        "BH",
        "BO",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IL",
        "IN",
        "IS",
        "IT",
        "JO",
        "JP",
        "KW",
        "LB",
        "LI",
        "LT",
        "LU",
        "LV",
        "MA",
        "MC",
        "MT",
        "MX",
        "MY",
        "NI",
        "NO",
        "NZ",
        "OM",
        "PA",
        "PE",
        "PH",
        "PL",
        "PS",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TN",
        "TR",
        "TW",
        "US",
        "UY",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/6vv7WBvEbdxi2cBk0sNDo6"
      },
      "href": "https://api.spotify.com/v1/albums/6vv7WBvEbdxi2cBk0sNDo6",
      "id": "6vv7WBvEbdxi2cBk0sNDo6",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273d2fcc575b19853f95741bbcf",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02d2fcc575b19853f95741bbcf",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851d2fcc575b19853f95741bbcf",
          "width": 64
        }
      ],
      "name": "Miles: The New Miles Davis Quintet (Rudy Van Gelder Remaster)",
      "release_date": "1956-04",
      "release_date_precision": "month",
      "total_tracks": 6,
      "type": "album",
      "uri": "spotify:album:6vv7WBvEbdxi2cBk0sNDo6"
    },
    {
      "album_group": "compilation",
      "album_type": "compilation",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "CA",
        "US"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/08u3rOc62Ho1JULrc4PJyO"
      },
      "href": "https://api.spotify.com/v1/albums/08u3rOc62Ho1JULrc4PJyO",
      "id": "08u3rOc62Ho1JULrc4PJyO",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273791b679ab2cdd56f519fb8e5",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02791b679ab2cdd56f519fb8e5",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851791b679ab2cdd56f519fb8e5",
          "width": 64
        }
      ],
      "name": "The Very Best Of The Miles Davis Quintet",
      "release_date": "2012-01-01",
      "release_date_precision": "day",
      "total_tracks": 10,
      "type": "album",
      "uri": "spotify:album:08u3rOc62Ho1JULrc4PJyO"
    },
    {
      "album_group": "compilation",
      "album_type": "compilation",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AD",
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BH",
        "BO",
        "BR",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JO",
        "JP",
        "KW",
        "LB",
        "LI",
        "LT",
        "LU",
        "LV",
        "MA",
        "MC",
        "MT",
        "MY",
        "NI",
        "NL",
        "NO",
        "NZ",
        "OM",
        "PA",
        "PE",
        "PH",
        "PL",
        "PS",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TH",
        "TN",
        "TR",
        "TW",
        "UY",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/3dS6aL7AigerKfPWGmo3NF"
      },
      "href": "https://api.spotify.com/v1/albums/3dS6aL7AigerKfPWGmo3NF",
      "id": "3dS6aL7AigerKfPWGmo3NF",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b27380de6874af755540ddd6cc9a",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e0280de6874af755540ddd6cc9a",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d0000485180de6874af755540ddd6cc9a",
          "width": 64
        }
      ],
      "name": "The Very Best Of The Miles Davis Quintet",
      "release_date": "2012-01-01",
      "release_date_precision": "day",
      "total_tracks": 10,
      "type": "album",
      "uri": "spotify:album:3dS6aL7AigerKfPWGmo3NF"
    },
    {
      "album_group": "compilation",
      "album_type": "compilation",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
          },
          "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
          "id": "71Ur25Abq58vksqJINpGdx",
          "name": "Miles Davis Quintet",
          "type": "artist",
          "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
        }
      ],
      "available_markets": [
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BH",
        "BO",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IE",
        "IN",
        "IS",
        "IT",
        "JO",
        "JP",
        "KW",
        "LB",
        "LT",
        "LU",
        "LV",
        "MA",
        "MT",
        "MX",
        "MY",
        "NI",
        "NO",
        "OM",
        "PA",
        "PE",
        "PH",
        "PL",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TH",
        "TN",
        "TR",
        "TW",
        "US",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/03XgJJkhiUfrhcYrL9mopm"
      },
      "href": "https://api.spotify.com/v1/albums/03XgJJkhiUfrhcYrL9mopm",
      "id": "03XgJJkhiUfrhcYrL9mopm",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b273c2d2e0b48952a4d88af90697",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e02c2d2e0b48952a4d88af90697",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d00004851c2d2e0b48952a4d88af90697",
          "width": 64
        }
      ],
      "name": "The Legendary Prestige Quintet Sessions",
      "release_date": "2006-01-01",
      "release_date_precision": "day",
      "total_tracks": 32,
      "type": "album",
      "uri": "spotify:album:03XgJJkhiUfrhcYrL9mopm"
    },
    {
      "album_group": "appears_on",
      "album_type": "compilation",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/0LyfQWJT6nXafLPZqxe9Of"
          },
          "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
          "id": "0LyfQWJT6nXafLPZqxe9Of",
          "name": "Various Artists",
          "type": "artist",
          "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
        }
      ],
      "available_markets": [
        "AE",
        "AR",
        "AT",
        "AU",
        "BE",
        "BG",
        "BH",
        "BO",
        "BR",
        "CA",
        "CH",
        "CL",
        "CO",
        "CR",
        "CY",
        "CZ",
        "DE",
        "DK",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "ES",
        "FI",
        "FR",
        "GB",
        "GR",
        "GT",
        "HK",
        "HN",
        "HU",
        "ID",
        "IE",
        "IL",
        "IN",
        "IS",
        "IT",
        "JP",
        "KW",
        "LB",
        "LT",
        "LU",
        "LV",
        "MA",
        "MT",
        "MX",
        "MY",
        "NI",
        "NL",
        "NO",
        "NZ",
        "OM",
        "PA",
        "PE",
        "PH",
        "PL",
        "PT",
        "PY",
        "QA",
        "RO",
        "SA",
        "SE",
        "SG",
        "SK",
        "SV",
        "TH",
        "TN",
        "TR",
        "TW",
        "US",
        "UY",
        "VN",
        "ZA"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/4YfZo49ctM0NJtk0nkUqqb"
      },
      "href": "https://api.spotify.com/v1/albums/4YfZo49ctM0NJtk0nkUqqb",
      "id": "4YfZo49ctM0NJtk0nkUqqb",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b27315fb61094dd9a56175fb2bd4",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e0215fb61094dd9a56175fb2bd4",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d0000485115fb61094dd9a56175fb2bd4",
          "width": 64
        }
      ],
      "name": "Jazz Music",
      "release_date": "2020-07-03",
      "release_date_precision": "day",
      "total_tracks": 69,
      "type": "album",
      "uri": "spotify:album:4YfZo49ctM0NJtk0nkUqqb"
    },
    {
      "album_group": "appears_on",
      "album_type": "compilation",
      "artists": [
        {
          "external_urls": {
            "spotify": "https://open.spotify.com/artist/0LyfQWJT6nXafLPZqxe9Of"
          },
          "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
          "id": "0LyfQWJT6nXafLPZqxe9Of",
          "name": "Various Artists",
          "type": "artist",
          "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
        }
      ],
      "available_markets": [
        "JP"
      ],
      "external_urls": {
        "spotify": "https://open.spotify.com/album/7tukMBBQUczmEoUu43MS8T"
      },
      "href": "https://api.spotify.com/v1/albums/7tukMBBQUczmEoUu43MS8T",
      "id": "7tukMBBQUczmEoUu43MS8T",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/ab67616d0000b27394362e8bb299b98fcfd68b57",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ab67616d00001e0294362e8bb299b98fcfd68b57",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/ab67616d0000485194362e8bb299b98fcfd68b57",
          "width": 64
        }
      ],
      "name": "仕事ジャズ – WORKING JAZZ-",
      "release_date": "2020-06-19",
      "release_date_precision": "day",
      "total_tracks": 27,
      "type": "album",
      "uri": "spotify:album:7tukMBBQUczmEoUu43MS8T"
    }
  ],
  "limit": 20,
  "next": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx/albums?offset=20&limit=20&include_groups=album,single,compilation,appears_on",
  "offset": 0,
  "previous": null,
  "total": 89
}
}

