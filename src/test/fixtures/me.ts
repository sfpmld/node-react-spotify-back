export const me = {
  data: {
    "country": "FR",
    "display_name": "Test Man",
    "email": "test@hotmail.com",
    "explicit_content": {
      "filter_enabled": false,
      "filter_locked": false
    },
    "external_urls": {
      "spotify": "https://open.spotify.com/user/1121387464"
    },
    "followers": {
      "href": null,
      "total": 6
    },
    "href": "https://api.spotify.com/v1/users/1121387464",
    "id": "1121387464",
    "images": [
      {
        "height": null,
        "url": "https://scontent-cdt1-1.xx.fbcdn.net/v/t1.0-1/c0.0.320.320a/p320x320/1625570_10203080351071803_3555928_n.jpg?_nc_cat=106&_nc_sid=0c64ff&_nc_oc=AQlYVoMColprPFbInR1ooJ4Qgp3U98JJvhf52x-fOGQtpwenO-ykMRByAtOBNSFQoio&_nc_ht=scontent-cdt1-1.xx&oh=c124bae320836e73831f96499a9a2ead&oe=5F2F2407",
        "width": null
      }
    ],
    "product": "open",
    "type": "user",
    "uri": "spotify:user:1121387464"
  }
}
