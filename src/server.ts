import { commonMiddlewares } from './middlewares';
import { errorHandlingMiddleware } from './middlewares';
import { App } from './helpers';
import { startGQLServer } from './graphql';

import oauthRoutes from './rest/routes/oauthRoutes';

const app = App.getInstance();

// middlewares
commonMiddlewares(app);

// graphql
app.use(startGQLServer());

// routes
app.use(oauthRoutes);

// general error handling
errorHandlingMiddleware(app);

export { app };
