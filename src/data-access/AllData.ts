import { AllRepository, searchInput } from '../constants';
import { fetcher, directFetcher } from '../helpers';

export class AllData implements AllRepository {
  search(token: string, question: searchInput) {
    return fetcher.get(`/search`, {
      params: question,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };

  searchMore(token: string, url: string) {
    return directFetcher.request({
      method: 'get',
      url,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };
}

