import { AlbumRepository } from '../constants';
import { fetcher } from '../helpers';
import {data_api} from '../constants';

export class AlbumData implements AlbumRepository {
  getAlbums(token: string, ids: string) {
    return fetcher.get(`/albums`, {
      params: {
        ids
      },
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };

  getAlbumsByArtist(token: string, artistId: string) {
    return fetcher.get(`/artists/${artistId}/albums`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };

  getAlbumById(token: string, id: string) {
    return fetcher.get(`/albums/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };

  // will serve mainly for pagination purpose with spotify previous and
  // next urls
  getAlbumsByUrl(token: string, url: string) {
    // removing base api url to catch only request params
    const params = url.replace(data_api, '');
    return fetcher.get(`/albums/${params}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };

}
