import { ArtistRepository } from '../constants';
import { fetcher } from '../helpers';
import {data_api} from '../constants';

export class ArtistData implements ArtistRepository {
  getArtistById(token: string, id: string) {
    return fetcher.get(`/artists/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };

  // will serve mainly for pagination purpose with spotify previous and
  // next urls
  getArtistsByUrl(token: string, url: string) {
    // removing base api url to catch only request params
    const params = url.replace(data_api, '');
    return fetcher.get(`/artists/${params}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };
}

