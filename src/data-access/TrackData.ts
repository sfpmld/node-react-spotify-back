import { TrackRepository } from '../constants';
import { fetcher } from '../helpers';
import {data_api} from '../constants';

export class TrackData implements TrackRepository {
  getArtistTopTracks(token: string, artistId: string, country: string) {
    return fetcher.get(`/artists/${artistId}/top-tracks`, {
      params: {
        country
      },
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };

  getTracksByAlbum(token: string, albumId: string) {
    return fetcher.get(`/albums/${albumId}/tracks`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };

  // will serve mainly for pagination purpose with spotify previous and
  // next urls
  getAlbumsByUrl(token: string, url: string) {
    // removing base api url to catch only request params
    const params = url.replace(data_api, '');
    return fetcher.get(`/tracks/${params}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };

}

