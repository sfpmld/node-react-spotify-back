import querystring from 'querystring';
import { AuthRepository, oauth_grant_type } from '../constants';
import {
  data_account_api,
  data_api_id,
  redirect_uri,
  data_api_secret,
  data_api,
} from '../constants';
import { directFetcher } from '../helpers';

export class AuthData implements AuthRepository {
  async getToken(authCode: string) {
    const settings = {
      method: 'post',
      url: `${data_account_api}/api/token`,
      data: querystring.stringify({
        code: authCode,
        grant_type: oauth_grant_type,
        redirect_uri,
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization:
          'Basic ' +
          Buffer.from(`${data_api_id}:${data_api_secret}`).toString('base64'),
      },
    };

    return await directFetcher.request(settings);
  }

  async getUserInfos(access_token: string) {
    const options: { [keys: string]: any } = {
      url: `${data_api}/me`,
      headers: { Authorization: 'Bearer ' + access_token },
      json: true,
    };

    // use the access token to access the Spotify Web API
    return await directFetcher.request(options);
  }
}
