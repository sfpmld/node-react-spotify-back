import { UserRepository } from '../constants';
import { fetcher } from '../helpers';

export class UserData implements UserRepository {
  getProfile(token: string) {
    if (!token) throw new Error('token is missing');
    return fetcher.get(`/me`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
  };
}

