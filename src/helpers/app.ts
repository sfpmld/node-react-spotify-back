import express, {
  Express as AppType,
  Handler as ControllerType,
  Request,
  Response,
  NextFunction,
} from 'express';

export class App {
  private static instance: AppType;

  static getInstance(): AppType {
    this.instance = this.instance ? this.instance : express();
    return this.instance;
  }
}

export { AppType, ControllerType, Request, Response, NextFunction };
