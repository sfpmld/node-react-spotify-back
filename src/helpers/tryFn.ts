import {httpStatusCode} from "../constants";
import {authenticationError} from "../errors/authentication";

export const tryAsyncFn = async (fn: Function) => {
  try {
    return await fn();
  } catch (err) {
    if (err.statusCode === httpStatusCode.UNHAUTORIZED) throw new authenticationError('Token has expired', err)
    throw err;
  }
};

export const tryPromise = async (expression: Promise<any>) => {
  try {
    return await expression;
  } catch (err) {
    if (err.statusCode === httpStatusCode.UNHAUTORIZED) throw new authenticationError('Token has expired', err)
    throw err;
  }
};

// async-await-to package approximation to avoid too much dependencies
export const tryAsyncAwait = async (expression: Promise<any>) => {
  let result: any;
  let error: Error | undefined;
  try {
    result = await expression;
  } catch (err) {
    error = err;
  }
  return [error, result];
};
