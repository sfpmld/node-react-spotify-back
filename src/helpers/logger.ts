import winston from 'winston';
// logs with colorized output
import { format } from 'winston';

export const consoleFormatter = () => format.combine(
  format.timestamp(),
  format.printf(info => {
    const { timestamp, level, message, stack } = info;

    let logMessage = message;

    // Write colorized logs.
    let printMessage = `[${level}] ${timestamp} ${logMessage}`;
    if (stack) printMessage += ` ${stack}`;
    return format.colorize().colorize(level, printMessage);
  })
);


const {
  LOG_LEVEL = 'info',
  SERVICE_NAME = 'spotify_proxy',
  SERVICE_VERSION = '1.0',
  ENVIRONMENT = 'dev'
} = process.env;


export const logger = winston.createLogger({
  level: LOG_LEVEL,
  format:  consoleFormatter(),
  defaultMeta: {
    service: SERVICE_NAME,
    version: SERVICE_VERSION,
    environment: ENVIRONMENT
  },
  transports: [new winston.transports.Console()]
});

