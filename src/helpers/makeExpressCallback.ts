import { ControllerType } from './app'
import { getTokenFromHeaders } from '../helpers';


function makeExpressCallback(controller: Function): ControllerType {
  return (req, res, next) => {
    const httpRequest = {
      path: req.path,
      originalUrl: req.originalUrl,
      method: req.method,
      params: req.params,
      query: req.query,
      body: req.body,
      // for authentication
      code: req.query.code,
      state: req.query.state,
      req,
      res,
      next,
      headers: {
        'Content-Type': req.get('Content-Type'),
        Referer: req.get('referer'),
        'User-Agent': req.get('User-Agent'),
        Authorization: getTokenFromHeaders(req)
      },
    };

    controller(httpRequest)
      .then((httpResponse: any) => {
        if (!httpResponse) return;
        if (httpResponse.headers) {
          res.set(httpResponse.headers);
        }
        // sending response to client
        res.status(httpResponse.statusCode).json(httpResponse.body);
      })
      .catch((err: Error) => next(err));
  };
}

export default makeExpressCallback;
