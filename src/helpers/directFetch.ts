import { IDirectFetcher, TData } from '../constants';
import { tryAsyncFn } from '../helpers';

// fetcher factory
export const makeDirectFetch = (fetcher: IDirectFetcher<TData[] | string>) => {
  return class fetch {
    public static request = async function (
      settings: Object
    ): Promise<any> {
      return tryAsyncFn(() => fetcher(settings));
    };
  };
};
