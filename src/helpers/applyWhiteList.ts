export const applyWhiteList = (whiteList: string[], obj: { [keys: string]: any }) => {

    let res!: { [keys: string]: any | any[] };
    // applying whiteList to row
    res = whiteList.reduce((prev, current) => {
      return {
        ...prev,
        [current]: obj[current]
      };
    }, {});

    return res;
}
