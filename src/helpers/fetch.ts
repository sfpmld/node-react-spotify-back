import { IFetcher, TData } from '../constants';
import { tryAsyncFn } from '../helpers';

// fetcher factory
export const makeFetch = (fetcher: IFetcher<TData[] | string>, url: string) => {
  return class fetch {
    public static get = async function (
      path: string,
      settings: Object
    ): Promise<any> {
      return tryAsyncFn(() => fetcher(`${url}${path}`, 'get', settings));
    };

    public static post = async function (
      path: string,
      settings: Object
    ): Promise<any> {
      return tryAsyncFn(() => fetcher(`${url}${path}`, 'post', settings));
    };
  };
};
