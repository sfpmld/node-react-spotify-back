import express from 'express';

export class Router {
  private static instance: express.Router;

  static getInstance(): express.Router {
    return this.instance ? this.instance : express.Router();
  }
}
