import { applyWhiteList } from './applyWhiteList';

const mockedWhiteList = ['id', 'name', 'playlist'];
const mockedRow = {
  id: '5dfg45444s',
  name: 'great-name',
  link: 'http://link.com',
  playlist: 'my badass playlist'
};
const mockedWhiteListed = { id: '5dfg45444s', name: 'great-name', playlist: 'my badass playlist'
};

describe('applyWhiteList utility', () => {
  it('should keep only white listed field', async () => {
    const res = applyWhiteList(mockedWhiteList, mockedRow)
    expect(res).toEqual(mockedWhiteListed);
  });
});
