import { Request } from 'express';

export function getTokenFromHeaders(req: Request): string {
  const authHeaders = req.get('Authorization');
  const token = authHeaders ? authHeaders.replace('Bearer ', '') : '';

  // enrich request object with token retrieved from headers
  req.token = token;

  return token;
}

