import axios from 'axios';
import { IFetcher, IDirectFetcher } from '../constants';
import { ErrorFactory } from '../errors';

export const axiosFetcher: IFetcher<any> = function (
  url: string,
  method: string,
  settings: Object
) {
  let res: Promise<any>;
  switch (method) {
    case 'get':
      res = axios.get(url, settings);
      break;
    case 'post':
      res = axios.post(url, settings);
      break;
    default:
      throw ErrorFactory.internal('bad method for axios fetcher');
  }
  return res;
};

export const axiosDirectFetcher: IDirectFetcher<any> = function (
  settings: Object
) {
  return axios(settings);
};
