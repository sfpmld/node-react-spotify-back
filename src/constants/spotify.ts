export const spotify_auth_scope = encodeURI('user-modify-playback-state user-read-currently-playing user-library-modify user-library-read');

export const spotify_auth_response_type = 'code';

export const spotify_auth_show_dialog = 'true';

export const spotify_auth_grant_type = 'authorization_code';
