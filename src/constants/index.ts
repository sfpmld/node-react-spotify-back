export * from './config';
export * from './httpStatusCode';
export * from './types';
export * from './spotify';

import {
  spotify_auth_response_type,
  spotify_auth_show_dialog,
  spotify_auth_scope,
  spotify_auth_grant_type,
} from './spotify';
import { data_account_api, data_api_id, redirect_uri } from './config';

export const oauth_login_url = `${data_account_api}/authorize?client_id=${data_api_id}&response_type=${spotify_auth_response_type}&redirect_uri=${redirect_uri}&scope=${spotify_auth_scope}&show_dialog=${spotify_auth_show_dialog}`;

export const oauth_grant_type = spotify_auth_grant_type;
