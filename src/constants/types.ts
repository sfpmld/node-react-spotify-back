import {
  ArtistData,
  UserData,
  TrackData,
  AuthData,
  AlbumData,
  AllData,
} from '../data-access';

export interface searchInput {
  q: string;
  type: string;
  market?: string;
  limit?: number;
  offset?: number;
}

export interface AllRepository {
  search: (token: string, question: searchInput) => Promise<Artist[] | Track[]>;
}

export interface ArtistRepository {
  getArtistById: (token: string, id: string) => Promise<Artist[]>;
  getArtistsByUrl: (token: string, url: string) => Promise<Artist[]>;
}

export interface AlbumRepository {
  getAlbums: (token: string, ids: string) => Promise<Album[]>;
  getAlbumsByArtist: (token: string, id: string) => Promise<Album[]>;
  getAlbumById: (token: string, id: string) => Promise<Album[]>;
}

export interface TrackRepository {
  getArtistTopTracks: (
    token: string,
    artistId: string,
    country: string
  ) => Promise<Track[]>;
  getTracksByAlbum: (token: string, id: string) => Promise<Track[]>;
}

export interface UserRepository {
  getProfile: (token: string) => Promise<User[]>;
}

export interface AuthRepository {
  getToken: (oauthCode: string) => Promise<string | any>;
}

export type GQLContext = {
  [key: string]:
    | string
    | typeof ArtistData
    | typeof AlbumData
    | typeof UserData
    | typeof TrackData
    | typeof AuthData
    | typeof AllData
    | undefined;
};

export enum ArtistTypes {
  items = 'object[]',
  previous = 'string',
  next = 'string',
  offset = 'number',
  limit = 'number',
  total = 'number',
}

export interface Artist {
  items: [ArtistTypes.items];
  previous: [ArtistTypes.previous];
  next: [ArtistTypes.next];
  offset: [ArtistTypes.offset];
  limit: [ArtistTypes.limit];
  total: [ArtistTypes.total];
}

export enum ArtistItemTypes {
  id = 'string',
  name = 'string',
  images = 'object[]',
  genres = 'object[]',
  href = 'string',
  uri = 'string',
}

export interface ArtistItem {
  id: [ArtistItemTypes.id];
  name: [ArtistItemTypes.name];
  images: [ArtistItemTypes.images];
  genres: [ArtistItemTypes.genres];
  href: [ArtistItemTypes.href];
  uri: [ArtistItemTypes.uri];
}

// Album types
export enum AlbumTypes {
  items = 'object[]',
  previous = 'string',
  next = 'string',
  offset = 'number',
  limit = 'number',
  total = 'number',
}

export interface Album {
  items: [AlbumTypes.items];
  previous: [AlbumTypes.previous];
  next: [AlbumTypes.next];
  offset: [AlbumTypes.offset];
  limit: [AlbumTypes.limit];
  total: [AlbumTypes.total];
}

export enum AlbumItemTypes {
  id = 'string',
  name = 'string',
  artists = 'object[]',
  images = 'object[]',
  total_tracks = 'number',
  type = 'string',
  release_date = 'string',
  href = 'string',
  uri = 'string',
}
export interface AlbumItem {
  id: [AlbumItemTypes.id];
  images: [AlbumItemTypes.images];
  total_tracks: [AlbumItemTypes.total_tracks];
  type: [AlbumItemTypes.type];
  release_date: [AlbumItemTypes.release_date];
  href: [AlbumItemTypes.href];
  uri: [AlbumItemTypes.uri];
}

// Track type
export enum TrackTypes {
  items = 'object[]',
  previous = 'string',
  next = 'string',
  offset = 'number',
  limit = 'number',
  total = 'number',
}

export interface Track {
  items: [TrackTypes.items];
  previous: [TrackTypes.previous];
  next: [TrackTypes.next];
  offset: [TrackTypes.offset];
  limit: [TrackTypes.limit];
  total: [TrackTypes.total];
}

export enum TrackItemTypes {
  id = 'string',
  name = 'string',
  track_number = 'number',
  duration_ms = 'number',
  artists = 'object[]',
  href = 'string',
  uri = 'string',
}

export interface TrackItem {
  id: [TrackItemTypes.id];
  name: [TrackItemTypes.name];
  track_number: [TrackItemTypes.track_number];
  duration_ms: [TrackItemTypes.duration_ms];
  artists: [TrackItemTypes.artists];
  href: [TrackItemTypes.href];
  uri: [TrackItemTypes.uri];
}

// User Types
export enum UserTypes {
  id = 'string',
  display_name = 'string',
  email = 'string',
  images = 'object[]',
  product = 'string',
  href = 'string',
  uri = 'string',
}
export interface User {
  id: [UserTypes.id];
  display_name: [UserTypes.display_name];
  email: [UserTypes.email];
  images: [UserTypes.images];
  product: [UserTypes.product];
  href: [UserTypes.href];
  uri: [UserTypes.uri];
}

// interface type for fetcher like axios or node-fetch
export interface IFetcher<T> {
  (url: string, method: string, settings: Object): Promise<T>;
}

export interface IDirectFetcher<T> {
  (settings: Object): Promise<T>;
}
// type of response data permitted
export type TData = Artist & AlbumItem & Track & User;
