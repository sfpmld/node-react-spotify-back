export const port = process.env.PORT;
export const data_api = process.env.SPOTIFY_API || '';
export const data_account_api = process.env.SPOTIFY_ACCOUNT_API || '';
export const data_api_id = process.env.SPOTIFY_ID;
export const data_api_secret = process.env.SPOTIFY_SECRET;
export const redirect_uri = process.env.REDIRECT_URI;
export const client_url = process.env.CLIENT_URL || '';
// list of http info to avoid when logging
export const loggerHeadersBlackList = ['authorization', 'csrf-token', 'cookie', 'set-cookie'];

export const loggerQueryBlackList =  ['code'];

