import { gql } from 'apollo-server-express';

export const search = gql`
  input searchInput {
    q: String!
    type: String!
    market: String
    limit: Int
    offset: Int
  }

  union searchResult = Artist | Track

  extend type Query {
    search(data: searchInput): searchResult
    searchMore(url: String!): searchResult
  }
`;


