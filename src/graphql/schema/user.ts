import { gql } from 'apollo-server-express';

export const user = gql`
  type User {
    id: String
    display_name: String
    email: String
    images: [Image]
    product: String
    href: String
    uri: String
  }

  extend type Query {
    getProfile: User
  }
`;


