import { gql } from 'apollo-server-express';

export const track = gql`
  type Track {
    tracks: [TrackItem]
    previous: String
    next: String
    offset: Int
    limit: Int
    total: Int
  }

  type TrackItem {
    id: String
    name: String
    track_number: Int
    duration_ms: Int
    artists: [ArtistItem]
    album: [AlbumItem]
    href: String
    uri: String
  }

  extend type Query {
    getArtistTopTracks(artistId: String!, country: String!): [TrackItem]
    getTracksByAlbum(albumId: String!): Track
    getTracksByUrl(url: String!): [Track]
  }
`;


