import { gql } from 'apollo-server-express';

export const album = gql`
  type Album {
    albums: [AlbumItem]
    previous: String
    next: String
    offset: Int
    limit: Int
    total: Int
  }

  type AlbumItem {
    id: String
    name: String
    artists: [ArtistItem]
    images:  [Image]
    total_tracks: Int
    type: String
    release_date: String
    href: String
    uri: String
  }

  extend type Query {
    getAlbums(ids: String!): [Album]
    getAlbumById(id: String!): AlbumItem
    getAlbumsByArtistId(artistId: String!): Album
    getAlbumsByUrl(url: String!): [Album]
  }
`;

