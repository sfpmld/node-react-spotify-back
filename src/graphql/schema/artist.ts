import { gql } from 'apollo-server-express';

export const artist = gql`
  type Artist {
    artists: [ArtistItem]
    previous: String
    next: String
    offset: Int
    limit: Int
    total: Int
  }

  type ArtistItem {
    id: String
    name: String
    images: [Image]
    genres: [String]
    href: String
    uri: String
  }

  extend type Query {
    getArtistById(id: String!): ArtistItem
    getArtistsByUrl(url: String!): [ArtistItem]
  }
`;
