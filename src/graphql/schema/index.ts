import { root } from './root';
import { album } from './album';
import { artist } from './artist';
import { search } from './search';
import { track } from './track';
import { user } from './user';
import { common } from './common';

const typeDefs = [root, album, artist, search, track, user, common];

export { typeDefs };
