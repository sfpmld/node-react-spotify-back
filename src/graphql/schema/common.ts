import { gql } from 'apollo-server-express';

export const common = gql`
  type Image {
    height: Int
    url: String
    width: Int
  }
`;



