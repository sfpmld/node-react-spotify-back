import { AlbumTypes, AlbumItemTypes } from '../../constants';
import { applyWhiteList } from '../../helpers';
import {tryAsyncFn, tryPromise} from '../../helpers';

const albumWhiteList = Object.keys(AlbumTypes);
const albumItemWhiteList = Object.keys(AlbumItemTypes);


export default {
  getAlbums: async (
    _root: any,
    { ids }: any,
    { token, AlbumData }: any,
    _info: any
  ) => {
    const albumRepo = new AlbumData();
    const rows = (await tryPromise(albumRepo.getAlbums(token, ids))) || [];
    if (rows.length < 0) return rows;

    const albums = rows.data.albums;
    return albums.map((row: any[]) => {
      // applying white list for field to select
      return applyWhiteList(albumWhiteList, row);
    });
  },

  getAlbumById: async (
    _root: any,
    { id }: any,
    { token, AlbumData }: any,
    _info: any
  ) => {
    const albumRepo = new AlbumData();
    const rows = (await tryPromise(albumRepo.getAlbumById(token, id))) || [];
    if (rows.length < 0) return rows;

    const albums = rows.data;
    // applying white list for field to select
    return applyWhiteList(albumItemWhiteList, albums);
  },

  getAlbumsByArtistId: async (
    _root: any,
    { artistId }: any,
    { token, AlbumData }: any,
    _info: any
  ) => {
    const albumRepo = new AlbumData();
    const rows = (await tryPromise(albumRepo.getAlbumsByArtist(token, artistId))) || [];
    if (rows.length < 0) return rows;

    const albums = applyWhiteList(albumWhiteList, rows.data);
    const albumItems = albums.items.map((row: any[]) => {
      // applying white list for field to select
      return applyWhiteList(albumItemWhiteList, row);
    });
    delete albums.items;
    return {
      ...albums,
      albums: albumItems,
    };
  },

  getAlbumsByUrl: async (
    _root: any,
    { url }: any,
    { token, AlbumData }: any,
    _info: any
  ) => {
    const albumRepo = new AlbumData();
    const rows = (await tryPromise(albumRepo.getAlbumsByUrl(token, url))) || [];
    if (rows.length < 0) return rows;
    const albums = rows.data.albums;
    return albums.map((row: any[]) => {
      // applying white list for field to select
      return applyWhiteList(albumItemWhiteList, row);
    });
  },
};
