import resolver from './track';
import { tracksByAlbumId } from '../../test/fixtures';
import { TrackItemTypes } from '../../constants';
import { objectFieldTypes } from '../../test/testUtils';

const mockArtistRepo = jest.fn();
mockArtistRepo.mockReturnValue({
  getTracksByAlbum: (token: string, albumId: string) => {
    return token && albumId ? tracksByAlbumId : null;
  },
});
const mockToken = 'token';
const mockAlbumId = 'id';

describe('GraphQL: Resolver : track', () => {
  it('should retrieve tracks data by album id, of type TrackItemTypes', async () => {
    const result = await resolver.getTracksByAlbum(
      {},
      { albumId: mockAlbumId },
      { token: mockToken, TrackData: mockArtistRepo },
      {}
    );

    const expectedField = Object.keys(TrackItemTypes).map(type => {
      if (type === 'items') return 'tracks'
      return type;
    });
    const expectedTypes = Object.values(TrackItemTypes);

    expect(Object.keys(result.tracks[0])).toEqual(expect.arrayContaining(expectedField));
    expect(objectFieldTypes(result.tracks[0])).toEqual(expect.arrayContaining(expectedTypes));
  });
});


