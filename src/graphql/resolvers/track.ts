import { TrackItemTypes, AlbumTypes } from '../../constants';
import { applyWhiteList } from '../../helpers';
import { tryPromise } from '../../helpers';

const trackItemWhiteList = Object.keys(TrackItemTypes);
const albumWhiteList = Object.keys(AlbumTypes);

export default {
  getTracksByAlbum: async (
    _root: any,
    { albumId }: any,
    { token, TrackData }: any,
    _info: any
  ) => {
    const TrackRepo = new TrackData();
    const rows =
      (await tryPromise(TrackRepo.getTracksByAlbum(token, albumId))) || [];
    if (rows.length < 0) return rows;

    const tracks = applyWhiteList(albumWhiteList, rows.data);
    const tracksItems = tracks.items.map((row: any[]) => {
      // applying white list for field to select
      return applyWhiteList(trackItemWhiteList, row);
    });
    delete tracks.items;
    return {
      ...tracks,
      tracks: tracksItems,
    };
  },
};
