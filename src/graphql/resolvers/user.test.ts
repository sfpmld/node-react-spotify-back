import resolver from './user';
import { me } from '../../test/fixtures';
import { UserTypes } from '../../constants';
import { objectFieldTypes } from '../../test/testUtils';

const mockUserRepo = jest.fn();
mockUserRepo.mockReturnValue({
  getProfile: (token: string) => {
    if (!token) throw new Error('Bad request');
    return me;
  },
});
const mockToken = 'token';

describe('GraphQL: Resolver : user', () => {
  it('should retrieve user infos profile of UserTypes', async () => {
    const result = await resolver.getProfile(
      {},
      {},
      { token: mockToken, UserData: mockUserRepo },
      {}
    );

    const expectedField = Object.keys(UserTypes);
    const expectedTypes = Object.values(UserTypes);

    expect(Object.keys(result)).toMatchObject(expectedField);
    expect(objectFieldTypes(result)).toMatchObject(expectedTypes);
  });
});
