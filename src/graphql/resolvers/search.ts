import { ArtistTypes, ArtistItemTypes, TrackTypes, TrackItemTypes, searchInput } from '../../constants';
import { applyWhiteList } from '../../helpers';
import {tryPromise} from '../../helpers';


const artistWhiteList = Object.keys(ArtistTypes);
const artistItemWhiteList = Object.keys(ArtistItemTypes);
const trackWhiteList = Object.keys(TrackTypes);
const trackItemWhiteList = Object.keys(TrackItemTypes);

const rawToPretty = (itemFlag: string, rows: any, whiteList: any, itemWhiteList: any) => {
  const target = applyWhiteList(whiteList, rows.data[itemFlag]);
  const targetItems = target.items.map((row: any[]) => {
    // applying white list for field to select
    return applyWhiteList(itemWhiteList, row);
  });
  delete target.items;
  return {
    ...target,
    [itemFlag]: targetItems,
  };
}

export default {
    search: async (
      _root: any,
      { data }: { data: searchInput },
      { token, AllData }: any,
      _info: any
    ) => {
      const searchRepo = new AllData();
      const rows = (await tryPromise(searchRepo.search(token, data))) || []; if (rows.data.length < 0) return rows;

      // album type
      if (data.type === 'artist') {
        // applying white list for field to select
        return rawToPretty('artists', rows, artistWhiteList, artistItemWhiteList);
        // track type
      } else {
        return rawToPretty('tracks', rows, trackWhiteList, trackItemWhiteList);
      }
    },

    searchMore: async (
      _root: any,
      { url }: {url: string},
      { token, AllData }: any,
      _info: any
    ) => {
      const searchRepo = new AllData();
      const rows = (await tryPromise(searchRepo.searchMore(token, url))) || [];
      if (rows.data.length < 0) return rows;

      // album type
      if (rows.data.artists) {
        // applying white list for field to select
        return rawToPretty('artists', rows, artistWhiteList, artistItemWhiteList);
        // track type
      } else {
        return rawToPretty('tracks', rows, trackWhiteList, trackItemWhiteList);
      }
    },
}
