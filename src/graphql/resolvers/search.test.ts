import resolver from './search';
import { searchedArtist, searchedMoreArtist } from '../../test/fixtures';
import { ArtistTypes, searchInput, ArtistItemTypes } from '../../constants';
import { objectFieldTypes } from '../../test/testUtils';

const mockSearchRepo = jest.fn();
mockSearchRepo.mockReturnValue({
  search: (token: string, data: searchInput) => {
    return token && data ? searchedArtist : null;
  },
  searchMore: (token: string, url: string) => {
    return token && url ? searchedMoreArtist : null;
  },
});
const mockToken = 'token';
const mockDataInput = {
  q: 'Miles Davis',
  type: 'artist'
};
const mockUrl = 'url';

describe('GraphQL: Resolver : search', () => {
  it('should retrieve artists by a given search term, with type ArtistTypes', async () => {
    const result = await resolver.search(
      {},
      { data: mockDataInput },
      { token: mockToken, AllData: mockSearchRepo },
      {}
    );

    const expectedField = Object.keys(ArtistTypes).map(type => {
      if (type === 'items') return 'artists'
      return type;
    });
    const expectedTypes = Object.values(ArtistTypes);

    expect(Object.keys(result)).toEqual(expect.arrayContaining(expectedField));
    expect(objectFieldTypes(result)).toEqual(expect.arrayContaining(expectedTypes));
  });

  it('should retrieve artists list by given url, with type ArtistTypes', async () => {
    const result = await resolver.searchMore(
      {},
      { url: mockUrl },
      { token: mockToken, AllData: mockSearchRepo },
      {}
    );

    const expectedField = Object.keys(ArtistItemTypes);
    const expectedTypes = Object.values(ArtistItemTypes);

    expect(typeof result).toBe("object");
    expect(Object.keys(result.artists[0])).toMatchObject(expectedField);
    expect(objectFieldTypes(result.artists[0])).toMatchObject(expectedTypes);
  });
});

