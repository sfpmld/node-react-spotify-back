import { UserTypes } from '../../constants';
import { applyWhiteList } from '../../helpers';
import { tryPromise } from '../../helpers';

const userWhiteList = Object.keys(UserTypes);

export default {
  getProfile: async (
    _root: any,
    _args: any,
    { token, UserData }: any,
    _info: any
  ) => {
    const UserRepo = new UserData();
    const rows = (await tryPromise(UserRepo.getProfile(token))) || [];
    if (rows.length < 0) return rows;

    const user = rows.data;
    // applying white list for field to select
    const result = applyWhiteList(userWhiteList, user);
    return result;
  },
};
