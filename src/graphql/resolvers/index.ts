import search from './search';
import artist from './artist';
import album from './album';
import track from './track';
import user from './user';

export const resolvers = {
  Query: {
    ...search,
    ...artist,
    ...album,
    ...track,
    ...user
  },
  searchResult: {
    __resolveType(
      data: any,
      _ctx: any,
      info: any
    ) {
      if (data.artists) {
        return info.schema.getType('Artist');
      }
      if (data.tracks) {
        return info.schema.getType('Track');
      }
      return null;
    },
  },
};
