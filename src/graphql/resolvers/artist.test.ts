import resolver from './artist';
import { artistById, searchedMoreArtist } from '../../test/fixtures';
import { ArtistItemTypes } from '../../constants';
import { objectFieldTypes } from '../../test/testUtils';

const mockArtistRepo = jest.fn();
mockArtistRepo.mockReturnValue({
  getArtistById: (token: string, id: string) => {
    return token && id ? artistById : null;
  },
  getArtistsByUrl: (token: string, url: string) => {
    return token && url ? searchedMoreArtist : null;
  },
});
const mockToken = 'token';
const mockId = 'id';
const mockUrl = 'url';

describe('GraphQL: Resolver : artist', () => {
  it('should retrieve artists data by id of type ArtistItemTypes', async () => {
    const result = await resolver.getArtistById(
      {},
      { id: mockId },
      { token: mockToken, ArtistData: mockArtistRepo },
      {}
    );

    const expectedField = Object.keys(ArtistItemTypes);
    const expectedTypes = Object.values(ArtistItemTypes);

    expect(Object.keys(result)).toMatchObject(expectedField);
    expect(objectFieldTypes(result)).toMatchObject(expectedTypes);
  });

  it('should retrieve artists list by given url, with type ArtistTypes', async () => {
    const result = await resolver.getArtistsByUrl(
      {},
      { url: mockUrl },
      { token: mockToken, ArtistData: mockArtistRepo },
      {}
    );

    const expectedField = Object.keys(ArtistItemTypes);
    const expectedTypes = Object.values(ArtistItemTypes);

    expect(Array.isArray(result)).toBe(true);
    expect(Object.keys(result[0])).toMatchObject(expectedField);
    expect(objectFieldTypes(result[0])).toMatchObject(expectedTypes);
  });
});

