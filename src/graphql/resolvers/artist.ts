import { ArtistItemTypes } from '../../constants';
import { applyWhiteList } from '../../helpers';
import { tryPromise } from '../../helpers';

const artistItemWhiteList = Object.keys(ArtistItemTypes);

export default {
  getArtistById: async (
    _root: any,
    { id }: any,
    { token, ArtistData }: any,
    _info: any
  ) => {
    if (!token || !id) throw new Error('Bad request');
    const artistRepo = new ArtistData();
    const rows = (await tryPromise(artistRepo.getArtistById(token, id))) || [];
    if (rows.length < 0) return rows;
    const artists = rows.data;
    // applying white list for field to select
    return applyWhiteList(artistItemWhiteList, artists);
  },

  getArtistsByUrl: async (
    _root: any,
    { url }: any,
    { token, ArtistData }: any,
    _info: any
  ) => {
    if (!token || !url) throw new Error('Bad request');
    const artistRepo = new ArtistData();
    const rows = (await tryPromise(artistRepo.getArtistsByUrl(token, url))) || [];
    if (rows.length < 0) return rows;
    const artists = rows.data.artists.items;
    return artists.map((row: any[]) => {
      // applying white list for field to select
      return applyWhiteList(artistItemWhiteList, row);
    });
  },
};
