import { Request } from 'express';
import { ApolloServer } from 'apollo-server-express';

import {
  UserData,
  AllData,
  AlbumData,
  ArtistData,
  TrackData,
} from '../data-access';
import { typeDefs } from './schema';
import { resolvers } from './resolvers';
import { getTokenFromHeaders } from '../helpers';
import { App } from '../helpers';
import { GQLLogger } from './plugins/GQLLogging';
import { GQLContext } from '../constants';

// graphql endpoint
const GQLServer = () => {
  return new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req }: { req: Request }): GQLContext => ({
      token: getTokenFromHeaders(req),
      UserData,
      AllData,
      AlbumData,
      TrackData,
      ArtistData,
    }),
    playground: true,
    plugins: [GQLLogger],
  });
};

// starting graphql server
export const startGQLServer = () => {
  const server = GQLServer();
  const app = App.getInstance();
  server.applyMiddleware({
    app,
  });
  console.log(`GQL Server running on on path ${server.graphqlPath}`);

  return server.getMiddleware();
};
