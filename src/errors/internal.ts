import { IError } from './types';

export class internalError extends Error implements IError {
  public trace: Error;
  constructor(public message: string) {
    super();
    this.trace = new Error(this.message);
  }
}

