import { authenticationError } from './authentication';
import { accessDeniedError } from './accessDenied';
import { internalError } from './internal';

import { IError } from './types';

// export error Factory
export class ErrorFactory {
  public static authentication(message: string, trace: Error): IError {
    return new authenticationError(message, trace);
  }

  public static accessDenied(message: string, trace: Error): IError {
    return new accessDeniedError(message, trace);
  }

  public static internal(message: string): IError {
    return new internalError(message);
  }
}

// helpers to check type of error thrown
export class isErrorType {
  public static authentication(err: Error): boolean {
    return err instanceof authenticationError;
  }

  public static accessDenied(err: Error): boolean {
    return err instanceof accessDeniedError;
  }

  public static internal(err: Error): boolean {
    return err instanceof internalError;
  }
}
