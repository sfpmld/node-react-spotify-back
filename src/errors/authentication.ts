import { IError } from './types';

export class authenticationError extends Error implements IError{
  constructor(public message: string, public trace: Error) {
    super();
  }
}

