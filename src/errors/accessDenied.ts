import { IError } from './types';

export class accessDeniedError extends Error implements IError {
  constructor(public message: string, public trace: Error) {
    super();
  }
}
