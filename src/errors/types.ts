export interface IError {
  message: string;
  trace: Error;
}
