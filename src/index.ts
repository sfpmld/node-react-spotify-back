import { app } from './server';
import { port } from './constants';

// to be use for either application or testing purpose
export const start = ( port: string | undefined ) => {
  return new Promise((resolve, reject) => {
    try {
      const server = app.listen(port, () => {
        resolve(server);
      })
    } catch (err) {
      reject(err);
    }
  })
}

// Starting Server
start(port)
  .then(() => console.log(`Listening on port ${port}`))
  .catch(err => console.log(`Server start failed: ${err}`));
